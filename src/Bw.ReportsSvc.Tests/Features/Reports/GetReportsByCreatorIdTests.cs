﻿using Bw.ReportSvc.Data;
using Bw.ReportSvc.Entities;
using Bw.ReportSvc.Features.Reports;

using FluentAssertions;

using Moq.AutoMock;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xunit;

namespace Bw.ReportsSvc.Tests.Features.Reports
{
    public class GetReportsByCreatorIdTests : UnitTestBase
    {
        [Fact]
        public async Task ReturnsOnlyReportsCreatedBySpecifiedUserId()
        {
            var mocker = new AutoMocker();

            // Arrange
            await using var context = base.GetDataContext();
            mocker.Use(typeof(ReportDataContext), context);

            /* add some reports */
            var userId = Guid.NewGuid();
            const int numberOfReportsToGenerateForUser = 5;
            for (var i = 0; i < numberOfReportsToGenerateForUser; i++)
            {
                await context.Reports.AddAsync(new Report()
                {
                    Id = Guid.NewGuid(),
                    CreatedByUserId = userId,
                    Coversheet = "unit-test-coversheet",
                    ReportText = "unit-test-reportText"
                });
            }

            /* add a report created by someone else */
            await context.Reports.AddAsync(new Report()
            {
                Id = Guid.NewGuid(),
                CreatedByUserId = Guid.NewGuid(),
                Coversheet = "unit-test-coversheet",
                ReportText = "unit-test-reportText"
            });

            await context.SaveChangesAsync(CancellationToken.None);

            var handler = mocker.CreateInstance<GetReportsByCreatorId.Handler>();

            // Act
            var results = await handler.Handle(new GetReportsByCreatorId.Query()
            {
                CreatorUserId = userId
            }, CancellationToken.None);

            // Assert
            results.Count().Should().Be(numberOfReportsToGenerateForUser, "should not return reports crated by someone else");
        }
    }
}
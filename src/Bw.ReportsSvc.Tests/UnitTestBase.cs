﻿using Bw.ReportSvc.Data;

using Microsoft.EntityFrameworkCore;

using System;

namespace Bw.ReportsSvc.Tests
{
    public abstract class UnitTestBase
    {
        public ReportDataContext GetDataContext()
        {
            var dbName = Guid.NewGuid().ToString()[..5];
            var options = new DbContextOptionsBuilder<ReportDataContext>()
                .UseInMemoryDatabase(dbName)
                .Options;
            return new ReportDataContext(options);
        }
    }
}
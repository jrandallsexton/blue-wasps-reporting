﻿using Bw.Eventing.Framework;
using Bw.ReportSvc.Entities;

using Microsoft.EntityFrameworkCore;

namespace Bw.ReportSvc.Data
{
    public class ReportDataContext : DbContext, IEventingDbContext
    {
        public ReportDataContext(DbContextOptions<ReportDataContext> options)
            : base(options) { }

        public DbSet<Report> Reports { get; set; }
        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
    }
}
﻿using System;

namespace Bw.ReportSvc.Model
{
    public class ReportModel
    {
        public Guid Id { get; set; }
        public string Coversheet { get; set; }
        public string ReportText { get; set; }
        public Guid CreatedByUserId { get; set; }
        public string CreatedBy { get; set; }
    }
}
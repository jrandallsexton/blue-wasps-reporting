﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;

namespace Bw.ReportSvc.Entities
{
    public class Report : EntityBase
    {
        public string Coversheet { get; set; }
        public string ReportText { get; set; }
        public DateTime? ReviewedUtc { get; set; }
        public Guid? ReviewedByUserId { get; set; }
        public DateTime? ApprovedUtc { get; set; }
        public Guid? ApprovedByUserId { get; set; }

        public class EntityConfiguration : IEntityTypeConfiguration<Report>
        {
            public void Configure(EntityTypeBuilder<Report> builder)
            {
                builder.ToTable("Report");
                builder.HasKey("Id");
            }
        }
    }
}
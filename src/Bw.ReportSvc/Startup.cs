using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bw.Eventing.Framework;
using Bw.Eventing.Framework.Common;
using Bw.Eventing.Framework.Infrastructure;
using Bw.ReportSvc.Data;
using Bw.ReportSvc.Features.EventHandlers;
using MediatR;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Bw.ReportSvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = Configuration;

            services.AddDbContext<ReportDataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ReportDataContext")));

            services.AddSingleton(config)
                .AddScoped<IProvideEventBus, AzureEventBus>()
                .AddSingleton<IBroadcastEvents, EventBroadcaster<ReportDataContext>>()
                .AddSingleton(provider => RegisterEventing(provider));

            services.AddMediatR(typeof(Startup).Assembly);

            services.AddHostedService<EventBroadcasterHost<ReportDataContext>>();

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BlueWasps Reporting", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BlueWasps Reporting v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private Dictionary<string, Func<IncomingEvent, CancellationToken, Task<object>>> RegisterEventing(IServiceProvider serviceProvider)
        {
            var events = new Dictionary<string, Func<IncomingEvent, CancellationToken, Task<object>>>()
            {
                {
                    nameof(TpsReportSubmitted),
                    async (message, cancellationToken) =>
                    {
                        var incomingEvent = message.EventData.FromJson<TpsReportSubmitted.Event>();
                        return await HandleEvent(serviceProvider, incomingEvent, cancellationToken);
                    }
                }
            };
            return events;
        }

        private async Task<object> HandleEvent(IServiceProvider serviceProvider, IRequest incomingEvent, CancellationToken cancellationToken)
        {
            using var scope = serviceProvider.CreateScope();
            var mediator = scope.ServiceProvider.GetService<IMediator>();
            return await mediator.Send(incomingEvent, cancellationToken);
        }
    }
}
﻿using Bw.Eventing.Events.Reporting;
using Bw.Eventing.Framework.Common;
using Bw.ReportSvc.Data;
using Bw.ReportSvc.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bw.ReportSvc.Features.EventHandlers
{
    public class TpsReportSubmitted
    {
        public class Event : TpsReportSubmittedEvent, IRequest
        {
            public Event(Guid correlationId, Guid causationId, EventingMethod method)
                : base(correlationId, causationId, method) { }
        }

        public class Handler : IRequestHandler<Event>
        {
            private readonly ILogger<TpsReportSubmitted> _logger;
            private readonly ReportDataContext _dataContext;

            public Handler(ILogger<TpsReportSubmitted> logger, ReportDataContext dataContext)
            {
                _logger = logger;
                _dataContext = dataContext;
            }

            public async Task<Unit> Handle(Event request, CancellationToken cancellationToken)
            {
                using var logScope = _logger.BeginScope("{reportId}", request.ReportId);

                _logger.LogInformation("Begin handler with {@request}", request);

                var report = await _dataContext.Reports
                    .FirstOrDefaultAsync(r => r.Id == request.ReportId, cancellationToken);

                if (report == null)
                {
                    report = new Report()
                    {
                        Id = request.ReportId,
                        Coversheet = request.Coversheet,
                        ReportText = request.ReportText,
                        CreatedUtc = DateTime.UtcNow,
                        CreatedByUserId = request.CreatedBy
                    };
                    await _dataContext.Reports.AddAsync(report, cancellationToken);
                }
                else
                {
                    report.Coversheet = request.Coversheet;
                    report.ReportText = request.ReportText;
                    report.ModifiedUtc = DateTime.UtcNow;
                    report.ModifiedByUserId = request.CreatedBy;
                }

                await _dataContext.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
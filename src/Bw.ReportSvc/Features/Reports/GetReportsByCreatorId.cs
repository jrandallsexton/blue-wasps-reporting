﻿using Bw.ReportSvc.Data;
using Bw.ReportSvc.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bw.ReportSvc.Features.Reports
{
    public class GetReportsByCreatorId
    {
        public class Dto
        {
            public Guid Id { get; set; }
            public string Coversheet { get; set; }
            public string ReportText { get; set; }
        }

        public class Query : IRequest<IEnumerable<Dto>>
        {
            public Guid CreatorUserId { get; set; }
        }

        public class Handler : IRequestHandler<Query, IEnumerable<Dto>>
        {
            private readonly ReportDataContext _context;

            public Handler(ReportDataContext context)
            {
                _context = context;
            }

            public async Task<IEnumerable<Dto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var reports = await _context.Reports.Where(r => r.CreatedByUserId == request.CreatorUserId)
                    .ToListAsync(cancellationToken);
                return reports.Select(r => Map(r));
            }

            private static Dto Map(Report report)
            {
                return report == null
                    ? null
                    : new Dto()
                    {
                        Id = report.Id,
                        Coversheet = report.Coversheet,
                        ReportText = report.ReportText
                    };
            }
        }
    }
}